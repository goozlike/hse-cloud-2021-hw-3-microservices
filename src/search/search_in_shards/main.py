
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
 
from server import Server
from service import SearchInShardsService

shards = os.environ.get('SHARDS')

def main():
    shards_urls = []
    for shard in shards.split(','):
        shards_urls.append('http://' + shard)

    search_in_shards = SearchInShardsService(shards=shards_urls)
    server = Server('search_in_shards', search_in_shards=search_in_shards)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
