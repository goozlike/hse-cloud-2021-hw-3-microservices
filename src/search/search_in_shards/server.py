from tokenize import group
from flask import Flask, request
from service import SearchInShardsService


class Server(Flask):
    def __init__(self, name: str, search_in_shards: SearchInShardsService):
        super().__init__(name)
        self._search_service = search_in_shards
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        text = request.args.get('text')
        user_data = {
            "age" : request.args.get('age'),
            "gender" : request.args.get('gender'),
        }
       
        sr = self._search_service.get_search_data(search_text=text,  user_data=user_data)
        
        return sr.to_json()

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8001, **kwargs)
