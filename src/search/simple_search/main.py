
import os, sys
shard_ind = int(os.environ.get('SHARD_IND'))

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
 
from server import Server
from service import SimpleSearchService
from settings import SEARCH_DOCUMENTS_DATA_FILES
from common.data_source import CSV



def main():
    simple_search = SimpleSearchService(CSV(SEARCH_DOCUMENTS_DATA_FILES[shard_ind - 1]))
    server = Server('simple_search_' + str(shard_ind), simple_search=simple_search)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
