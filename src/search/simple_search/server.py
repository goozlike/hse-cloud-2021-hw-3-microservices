from tokenize import group
from flask import Flask, request
from service import SimpleSearchService
import os
shard_ind = int(os.environ.get('SHARD_IND'))

class Server(Flask):
    def __init__(self, name: str, simple_search: SimpleSearchService):
        super().__init__(name)
        self._simple_search = simple_search
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        text = request.args.get('text')
        user_data = {
            "age" : request.args.get('age'),
            "gender" : request.args.get('gender'),
        }
       
        sr = self._simple_search.get_search_data(search_text=text,  user_data=user_data)
        
        return sr.to_json()

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8010 + shard_ind, **kwargs)
