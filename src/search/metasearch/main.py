from server import Server
from service import MetaSearchService
import os

search = os.environ.get('SEARCH')
user_service = os.environ.get('USER_SERVICE')

def main():
    metasearch = MetaSearchService(search, user_service)
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
