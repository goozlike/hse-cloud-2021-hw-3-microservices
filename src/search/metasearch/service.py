from typing import List, Dict
import requests
import pandas as pd
import json

DOCS_COLUMNS = ['document', 'key', 'key_md5']

class MetaSearchService:
    def __init__(self, search: str, user_service: str):
        self._search = "http://" + search
        self._user_service = "http://" + user_service

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        user_data = json.loads(self.get_user_data(user_id))  # {'gender': ..., 'age': ...}
        df = pd.read_json(self.get_search_data(search_text, user_data, limit))
        return df[DOCS_COLUMNS].to_dict('records')

    def get_user_data(self, user_id):
        return requests.get(self._user_service + '/user?user_id=' + str(user_id)).content

    def get_search_data(self, search_text, user_data, limit):
        return requests.get(self._search + '/search?text=' + search_text + '&age=' + str(user_data["age"]) + '&gender=' + user_data["gender"]).content
