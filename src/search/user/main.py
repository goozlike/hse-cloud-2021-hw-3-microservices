import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
 
from server import Server
from service import UserService
from common.data_source import CSV
from settings import USER_DATA_FILE


def main():
    user_service = UserService(CSV(USER_DATA_FILE))
    server = Server('user_service', user_service=user_service)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
