from tokenize import group
from flask import Flask, request
from service import UserService
import json

class Server(Flask):
    def __init__(self, name: str, user_service: UserService):
        super().__init__(name)
        self._user_service = user_service
        urls = [
            ('/user', self.get_user, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_user(self):
        user_id = request.args.get('user_id')
        ur = self._user_service.get_user_data(user_id)
        return json.dumps(ur)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8002, **kwargs)
